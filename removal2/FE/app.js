const app = Vue.createApp({
    data() {
        return {
            fname: "",
            lname: "",
            mname: "",
            email: "",
            password: "",
            conpassword: "",
            error: "",
            show: null,
            loggedIn: null,
            user_name: ""
        }
    },
    methods: {
        register() {
            var valid = this.validateInputs();
            if (valid) {
                var formData = new FormData();
                formData.append('fname', this.fname);
                formData.append('lname', this.lname);
                formData.append('mname', this.mname);
                formData.append('email', this.email);
                formData.append('password', this.password);
                formData.append('conpassword', this.conpassword);
                instance.post('/register',
                    formData
                ).then(({
                    data
                }) => {
                    alert("Registered!")
                    this.clearInputs()
                }).catch((err) => {
                    alert("Failed!")
                });
            } else {
                this.error = "Please input all fields"
            }
        },
        validateInputs() {
            var valid = true;
            var emailRegex = /^[^\s@]+@[^\s@]+$/;
            if (this.password !== this.conpassword || this.password == "" || this.password.length < 3) {
                this.$refs.password.classList.add("is-invalid")
                this.$refs.conpassword.classList.add("is-invalid")
                valid = false
            } else {
                this.$refs.password.classList.remove("is-invalid")
                this.$refs.conpassword.classList.remove("is-invalid")
            }

            if (!emailRegex.test(this.email)){
                this.$refs.email.classList.add("is-invalid")
                valid = false
            }
            else
                this.$refs.email.classList.remove("is-invalid")

            if (this.fname == "") {
                this.$refs.fname.classList.add("is-invalid")
                valid = false
            } else
                this.$refs.fname.classList.remove("is-invalid")

            if (this.lname == "") {
                this.$refs.lname.classList.add("is-invalid")
                valid = false
            } else
                this.$refs.lname.classList.remove("is-invalid")


            if (this.mname == "") {
                this.$refs.mname.classList.add("is-invalid")
                valid = false
            } else
                this.$refs.mname.classList.remove("is-invalid")

            return valid;
        },
        login() {
            var formData = new FormData();
            formData.append('email', this.email);
            formData.append('password', this.password);
            instance.post('/login',
                formData
            ).then(({
                data
            }) => {
                console.log(data)
                this.loggedIn = true
                this.show = null
                localStorage.setItem("access_token", data.access_token);
                this.clearInputs()
                this.name = data.user.fname + " " + data.user.mname + " " + data.user.lname
                setHeaders()
            }).catch((err) => {
                if (err.response.status == "422" || err.response.status == "401")
                    this.error = "Incorrect username or password."
            });
        },
        logout() {
            this.show = true
            this.loggedIn = false
            instance.post('/logout').then(({
                data
            }) => {
                this.show = true
                this.loggedIn = false
                localStorage.removeItem("access_token")
                window.location.href='register.html'
                //window.location.reload()
            }).catch((err) => {
                if (err.response.status == "422")
                    this.error = "Incorrect username or password."
            });
        },
        refresh() {
            instance.post('/refresh').then(({
                data
            }) => {
                console.log(data)
                localStorage.setItem("access_token", data.access_token);
                setHeaders()
            }).catch((err) => {});
        },
        showRegister() {
            this.show = false
            this.clearInputs()
        },
        showLogin() {
            this.show = true
            this.clearInputs()
        },
        clearInputs() {
            this.fname = ""
            this.lname = ""
            this.mname = ""
            this.email = ""
            this.password = ""
            this.conpassword = ""
            this.error = ""
        },
        getUserData() {
            instance.get('/user-profile').then(({
                data
            }) => {
                this.name = data.fname + " " + data.mname + " " + data.lname
                this.loggedIn = true
                this.show = null
            }).catch((err) => {
                console.log(err)
                this.show = true
                this.loggedIn = false
            });
        }
    },
    beforeMount() {
        this.getUserData()
        this.refresh()
    }
});

app.mount("#app")
