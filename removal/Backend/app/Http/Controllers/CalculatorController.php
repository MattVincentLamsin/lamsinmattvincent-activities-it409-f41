<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\History;

class CalculatorController extends Controller
{
        public function addhistory() {
        $history = new History();
        $history->numbers = request('numbers');
        $history->answer = request('answer');
        $history->save();
        return;
    }
    
     public function gethistory() {
        $history = History::orderBy('id', 'desc')->get();
        return $history;
    }
}
