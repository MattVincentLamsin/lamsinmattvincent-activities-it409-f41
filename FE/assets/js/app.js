const EventHandling = {
    data() {
        return {
            header: "All Songs",
            playlistarray: [],
            addp: "",
            upload: [],
            song: [
                {
                    title: "Some title",
                    artist: "Some artist",
                    album: "Some album",
                    duration: "3:45"
                },
                {
                    title: "Some title",
                    artist: "Some artist",
                    album: "Some album",
                    duration: "3:45"
                },
                {
                    title: "Some title",
                    artist: "Some artist",
                    album: "Some album",
                    duration: "3:45"
                },
                {
                    title: "Some title",
                    artist: "Some artist",
                    album: "Some album",
                    duration: "3:45"
                },
                {
                    title: "Some title",
                    artist: "Some artist",
                    album: "Some album",
                    duration: "3:45"
                },
                {
                    title: "Some title",
                    artist: "Some artist",
                    album: "Some album",
                    duration: "3:45"
                },
                {
                    title: "Some title",
                    artist: "Some artist",
                    album: "Some album",
                    duration: "3:45"
                },
                {
                    title: "Some title",
                    artist: "Some artist",
                    album: "Some album",
                    duration: "3:45"
                }

            ],
            test: ''
        }
    },
    methods: {
        changeText() {
            this.header = "Playlistname"
        },

        addsong() {
            this.song.push({
                title: "aaaa",
                artist: "artist",
                album: "album",
                duration: "3:14"
            })
        },

        addplaylist() {
            this.playlistarray.push({
                playlistname: this.addp
            })
        },

        drop(e) {
            e.preventDefault()
            this.upload = e.dataTransfer.files
        },

        drag(e) {
            e.preventDefault()
        },

        up() {
            var title = "";
            var artist = "";
            var album = "";
            var duration = "";
            var jsmediatags = window.jsmediatags;
            var send = this;
            var fle = this.upload;

            jsmediatags.read(fle[0], {
                onSuccess: function (tag) {

                    var tag = tag.tags
                    if (tag.title) {
                        title = tag.title
                    }
                    else{
                        title=fle[0].name
                    }
                    if (tag.artist) {
                        artist = tag.artist
                    }
                    if (tag.album) {
                        album = tag.album
                    }

                    var src = URL.createObjectURL(fle[0]);
                    var audio = document.querySelector("#mp3");
                    audio.src = src;
                    audio.onloadedmetadata = function () {

                        duration = send.time(audio.duration);
                        send.song.push({
                            title: title,
                            artist: artist,
                            album: album,
                            duration: duration
                        })
                        document.querySelector(".cancel").click()
                    };
                },


                onError: function (error) {
                    console.log(error);
                }
            });
            f
        },

        time(duration) {

            var hrs = ~~(duration / 3600);
            var mins = ~~((duration % 3600) / 60);
            var secs = ~~duration % 60;


            var ret = "";

            if (hrs > 0) {
                ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
            }

            ret += "" + mins + ":" + (secs < 10 ? "0" : "");
            ret += "" + secs;

            return ret;
        }

    }
}

Vue.createApp(EventHandling).mount('#head')
