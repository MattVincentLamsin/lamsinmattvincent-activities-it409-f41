const EventHandling = {
    data() {
        return {
            notinplaylistsongs: [],
            sidebar: false,
            renameplaylist: "",
            header: "All Songs",
            playlstindex: null,
            playlistarray: [],
            addp: "",
            upload: [],
            song: [],
            test: ''
        }
    },
    methods: {
        changeText() {
            this.header = "Playlistname"
        },

        show() {
            this.sidebar = true
        },

        hide() {
            this.sidebar = false
        },


        deletesong(i) {
            var send = this
            var id = ""
            var link = ""
            if (this.playlstindex == null) {
                id = this.song[i].id
                link = "deletesong"
            } else {
                id = this.song[i].ps_id
                link = "deletesongfromplaylist"
            }
            var formData = new FormData();
            formData.append('id', id);

            axios.post('http://127.0.0.1:8000/' + link,
                formData
            ).then(({
                data
            }) => {
                send.song.splice(i, 1)
            }).catch((err) => {});
        },

        addplaylist() {
            var send = this
            var formData = new FormData();
            formData.append('name', send.addp);

            axios.post('http://127.0.0.1:8000/addplaylist',
                formData
            ).then(({
                data
            }) => {
                console.log(data);
                send.playlistarray.push({
                    id: data,
                    playlistname: send.addp
                })
            }).catch((err) => {});

        },

        showplaylist(i) {
            this.playlstindex = i
            this.header = this.playlistarray[i].playlistname
            this.renameplaylist = this.header
            this.showplaylistsongs()
        },

        addsongtop(i) {
            var song_id = this.notinplaylistsongs[i].id
            var playlist_id = this.playlistarray[this.playlstindex].id
            var send = this
            var formData = new FormData();
            formData.append('song_id', song_id);
            formData.append('playlist_id', playlist_id);

            axios.post('http://127.0.0.1:8000/addsongtoplaylist',
                formData
            ).then(({
                data
            }) => {
                console.log(data);
                send.song.push({
                    id: send.notinplaylistsongs[i].id,
                    title: send.notinplaylistsongs[i].title,
                    artist: send.notinplaylistsongs[i].artist,
                    duration: send.notinplaylistsongs[i].duration
                })
                send.notinplaylistsongs.splice(i, 1)
            }).catch((err) => {});
        },

        showplaylistsongs() {
            this.song = []
            var send = this
            var i = this.playlstindex
            var id = this.playlistarray[i].id
            axios.get('http://127.0.0.1:8000/displaysongtoplaylist/' + id)
                .then(({
                    data
                }) => {
                    for (i = 0; i < data.length; i++) {
                        send.song.push({
                            id: data[i].id,
                            title: data[i].title,
                            artist: data[i].artist,
                            duration: send.time(data[i].length),
                            ps_id: data[i].ps_id
                        })
                    }
                }).catch((err) => {});
        },

        shownotinplaylistsongs() {
            this.notinplaylistsongs = []
            var send = this
            var i = this.playlstindex
            var id = this.playlistarray[i].id
            axios.get('http://127.0.0.1:8000/displaysongnotinplaylist/' + id)
                .then(({
                    data
                }) => {
                    console.log(data);
                    for (i = 0; i < data.length; i++) {
                        send.notinplaylistsongs.push({
                            id: data[i].id,
                            title: data[i].title,
                            artist: data[i].artist,
                            album: data[i].album,
                            duration: send.time(data[i].length)
                        })
                    }
                }).catch((err) => {});
        },

        savechanges() {
            var i = this.playlstindex
            this.playlistarray[i].playlistname = this.renameplaylist
            this.header = this.renameplaylist

            var send = this
            var id = this.playlistarray[i].id
            var name = this.renameplaylist
            var formData = new FormData();
            formData.append('name', name);
            formData.append('id', id);

            axios.post('http://127.0.0.1:8000/editplaylist',
                formData
            ).then(({
                data
            }) => {
                console.log(data);
                this.renameplaylist = ""
            }).catch((err) => {});

        },

        deleteplaylist() {
            var i = this.playlstindex
            var send = this
            var id = this.playlistarray[i].id
            var formData = new FormData();
            formData.append('id', id);

            axios.post('http://127.0.0.1:8000/deleteplaylist',
                formData
            ).then(({
                data
            }) => {
                console.log(data);
                send.playlistarray.splice(i, 1)
                window.location.reload()
            }).catch((err) => {});

        },

        drop(e) {
            e.preventDefault()
            this.upload = e.dataTransfer.files
        },

        drag(e) {
            e.preventDefault()
        },

        up() {
            var title = "-----------";
            var artist = "-----------";
            var album = "-----------";
            var duration = "-----------";
            var jsmediatags = window.jsmediatags;
            var send = this;
            var fle = this.upload;

            jsmediatags.read(fle[0], {
                onSuccess: function (tag) {
                    var tag = tag.tags
                    console.log(tag);
                    if (tag.title) {
                        title = tag.title
                    } else {
                        var t = fle[0].name.split('.').slice(0, -1);
                        title = t[0];
                    }
                    if (tag.artist) {
                        artist = tag.artist
                    }
                    if (tag.album) {
                        album = tag.album
                    }
                    console.log(artist);

                    var src = URL.createObjectURL(fle[0]);
                    var audio = document.querySelector("#mp3");
                    audio.src = src;
                    audio.onloadedmetadata = function () {

                        var formData = new FormData();
                        duration = Math.round((audio.duration + Number.EPSILON) * 100) / 100;
                        formData.append('title', title);
                        formData.append('artist', artist);
                        formData.append('length', duration);
                        formData.append('album', album);
                        formData.append('song', fle[0]);


                        axios.post('http://127.0.0.1:8000/addsong',
                            formData, {
                                headers: {
                                    'Content-Type': 'multipart/form-data'
                                }
                            }
                        ).then(({
                            data
                        }) => {
                            console.log(data);
                            duration = send.time(audio.duration);
                            send.song.push({
                                title: title,
                                artist: artist,
                                album: album,
                                duration: duration
                            })
                            document.querySelector(".cancel").click()
                        }).catch((err) => {});

                    };
                },


                onError: function (error) {
                    console.log(error);
                }
            });
        },

        time(duration) {

            var hrs = ~~(duration / 3600);
            var mins = ~~((duration % 3600) / 60);
            var secs = ~~duration % 60;


            var ret = "";

            if (hrs > 0) {
                ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
            }

            ret += "" + mins + ":" + (secs < 10 ? "0" : "");
            ret += "" + secs;

            return ret;
        },

        displaysong() {
            this.song = []
            var send = this
            this.playlstindex = null
            this.header = "All Songs"
            axios.get('http://127.0.0.1:8000/displaysong')
                .then(({
                    data
                }) => {
                    for (i = 0; i < data.length; i++) {
                        send.song.push({
                            id: data[i].id,
                            title: data[i].title,
                            artist: data[i].artist,
                            album: data[i].album,
                            duration: send.time(data[i].length)
                        })
                    }
                }).catch((err) => {});
        },

        displayplaylist() {
            var send = this
            axios.get('http://127.0.0.1:8000/displayplaylist')
                .then(({
                    data
                }) => {
                    for (i = 0; i < data.length; i++) {
                        send.playlistarray.push({
                            id: data[i].id,
                            playlistname: data[i].name
                        })
                    }
                }).catch((err) => {});
        }
    },

    beforeMount() {
        this.displaysong()
        this.displayplaylist()
    }
}

Vue.createApp(EventHandling).mount('#head')
