<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SongController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::post('/addsong', [SongController::class, 'addsong']);
Route::get('/displaysong', [SongController::class, 'displaysong']);
Route::post('/deletesong', [SongController::class, 'deletesong']);


Route::post('/addplaylist', [SongController::class, 'addplaylist']);
Route::get('/displayplaylist', [SongController::class, 'displayplaylist']);
Route::post('/deleteplaylist', [SongController::class, 'deleteplaylist']);
Route::post('/editplaylist', [SongController::class, 'editplaylist']);


Route::post('/addsongtoplaylist', [SongController::class, 'addsongtoplaylist']);
Route::get('/displaysongtoplaylist/{id}', [SongController::class, 'displaysongtoplaylist']);
Route::post('/deletesongfromplaylist', [SongController::class, 'deletesongfromplaylist']);
Route::get('/displaysongnotinplaylist/{id}', [SongController::class, 'displaysongnotinplaylist']);



    